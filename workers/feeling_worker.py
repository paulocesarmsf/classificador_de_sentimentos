import json
from models.feeling import Feeling


class FeelingWorker(object):

    def create_feeling(self, texts, labels):
        feeling = Feeling(texts=texts, labels=labels)
        feeling.save()
        return {'id': feeling.feeling_id,
                'texts': feeling.texts,
                'labels': feeling.labels}

    def get_feeling(self, id):
        feeling = Feeling(feeling_id=id)
        return json.loads(feeling.to_json())
