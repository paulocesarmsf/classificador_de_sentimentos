from sklearn import metrics


class MetricsWorker(object):

    def get_metrics(self, y_true, y_pred):
        return {"precision_macro": str(metrics.precision_score(y_true, y_pred, average='macro')),
                "precision_micro": str(metrics.precision_score(y_true, y_pred, average='micro')),
                "precision_weighted": str(metrics.precision_score(y_true, y_pred, average='weighted'))}
