from sklearn.model_selection import train_test_split
import pandas as pd

from config import DATAS_DIR
from workers.metrics_worker import MetricsWorker


class TrainingWorker(object):

    def __init__(self, clf, vetorizor):
        self.clf = clf
        self.vetorizor = vetorizor
        self.datas_path = DATAS_DIR
        self.metrics_worker = MetricsWorker()

    def get_df_training(self, name_dataframe):
        return pd.read_csv(self.datas_path + '{name_dataframe}.csv'.format(name_dataframe=name_dataframe))

    def get_data_training(self, df, key_text, key_labels, test_size):
        texts = []
        labels = []

        for index, row in df.iterrows():
            texts.append(row[key_text])
            labels.append(row[key_labels])

        text_vetorized = self.vetorizor.fit_transform(texts)
        X_train, X_test, y_train, y_test = train_test_split(text_vetorized, labels, test_size=test_size)
        return X_train, X_test, y_train, y_test

    def training_model(self, df_name, key_texts, key_labels, percent_test):
        X_train, X_test, y_train, y_test = self.get_data_training(self.get_df_training(df_name),
                                                                  key_texts, key_labels, percent_test)
        self.clf.fit(X_train, y_train)

        y_pred = list(self.clf.predict(X_test))
        y_true = y_test
        print(self.metrics_worker.get_metrics(y_true, y_pred))

        return self.clf, self.vetorizor

    def print_results(self, y_pred, y_true):
        print("Resultados obtidos: ")
        print(y_pred)
        print("\n Resultados esperados: ")
        print(y_true)
