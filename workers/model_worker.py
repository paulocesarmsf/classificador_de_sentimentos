from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import BernoulliNB
import pickle

from config import MODELS_DIR
from models.feeling import Feeling
from workers.feeling_worker import FeelingWorker


class ModelWorker(object):

    def __init__(self):
        self.models_path = MODELS_DIR
        self.feeling_worker = FeelingWorker()

    def generate_naive_bayes(self):
        return BernoulliNB(), CountVectorizer(binary='true')

    def analisar_frase(self, classificador, vetorizador, frase):
        labels = classificador.predict(vetorizador.transform([frase]))
        feeling = self.feeling_worker.create_feeling([frase], [str(list(labels)[0])])
        print(self.feeling_worker.get_feeling(feeling.get('id')))
        return feeling

    def analisar_lista_de_frases(self, classificador, vetorizador, lista_de_frases):
        labels = classificador.predict(vetorizador.transform(lista_de_frases))
        feeling = self.feeling_worker.create_feeling(lista_de_frases, ','.join(str(e) for e in list(labels)).split(','))
        print(self.feeling_worker.get_feeling(feeling.get('id')))
        return feeling

    def save_model(self, classificador, vetorizador, models_name):
        with open(self.models_path + '{models_name}.pkl'.format(models_name=models_name), 'wb') as fout:
            pickle.dump((vetorizador, classificador), fout)

    def open_model(self, models_name):
        with open(self.models_path + '{models_name}.pkl'.format(models_name=models_name), 'rb') as fin:
            vectorizer, clf = pickle.load(fin)
            return clf, vectorizer
