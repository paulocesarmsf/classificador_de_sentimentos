import datetime
import uuid
from mongoengine import Document, StringField, ListField, DateTimeField


class Feeling(Document):
    feeling_id = StringField(default=lambda: uuid.uuid4().hex, max_length=200, required=True)
    texts = ListField(default=[])
    labels = ListField(default=[])
    created = DateTimeField(default=datetime.datetime.now())
