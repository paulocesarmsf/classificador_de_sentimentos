import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
MODELS_DIR = os.path.dirname(os.path.abspath(__file__)) + '/models_files/'
DATAS_DIR = os.path.dirname(os.path.abspath(__file__)) + '/datas/'
