from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split

import pandas as pd

from workers.metrics_worker import MetricsWorker
from workers.model_worker import ModelWorker

model_worker = ModelWorker()
training = MetricsWorker()

# clf = BernoulliNB()
clf = MultinomialNB()
vetorizador = CountVectorizer(binary='true')

df = pd.read_csv('../datas/pontofrio_labelled_stopwords.csv')

treino_comentarios = []
treino_respostas = []
for index, row in df.iterrows():
    treino_comentarios.append(row['comentarios'])
    treino_respostas.append(row['respostas'])

treino_comentarios = vetorizador.fit_transform(treino_comentarios)


X_train, X_test, y_train, y_test = train_test_split(treino_comentarios, treino_respostas, test_size=0.3)


clf.fit(X_train, y_train)

y_pred = list(clf.predict(X_test))
y_true = y_test

print("Resultados obtidos: ")
print(y_pred)
print("\n Resultados esperados: ")
print(y_true)
print(training.get_metrics(y_true, y_pred))

#
# def analisar_frase(classificador, vetorizador, frase):
#     return frase, classificador.predict(vetorizador.transform([frase]))
#
#
# print(analisar_frase(clf,
#                      vetorizador,
#                      'produto de ótima qualidade, só tenho pequena restrição quanto ao controle q poderia ser melhor.'))
#
model_worker.save_model(clf, vetorizador, 'pontofrio_labelled_v3')
# clf, vetorizador = model_worker.open_model('comment_feeling_stopwords')
# print(analisar_frase(clf,
#                      vetorizador,
#                      'produto de ótima qualidade, só tenho pequena restrição quanto ao controle q poderia ser melhor.'))
