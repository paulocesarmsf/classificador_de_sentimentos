
class PreProcessador(object):

    def obter_dados_das_fontes(self, diretorio_base):
        with open(diretorio_base + "imdb_labelled.txt", "r") as arquivo_texto:
            dados = arquivo_texto.read().split('\n')

        with open(diretorio_base + "amazon_cells_labelled.txt", "r") as arquivo_texto:
            dados += arquivo_texto.read().split('\n')

        with open(diretorio_base + "yelp_labelled.txt", "r") as arquivo_texto:
            dados += arquivo_texto.read().split('\n')

        return dados

    def tratamento_dos_dados(self, dados):
        dados_tratados = []
        for dado in dados:
            if len(dado.split("\t")) == 2 and dado.split("\t")[1] != "":
                dados_tratados.append(dado.split("\t"))

        return dados_tratados

    def dividir_dados_para_treino_e_validacao(self, dados):
        quantidade_total = len(dados)
        percentual_para_treino = 0.75
        treino = []
        validacao = []

        for indice in range(0, quantidade_total):
            if indice < quantidade_total * percentual_para_treino:
                treino.append(dados[indice])
            else:
                validacao.append(dados[indice])

        return treino, validacao

    def pre_processamento(self):
        dados = self.obter_dados_das_fontes("datas/")
        dados_tratados = self.tratamento_dos_dados(dados)

        return self.dividir_dados_para_treino_e_validacao(dados_tratados)
