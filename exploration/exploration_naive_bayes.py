from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import BernoulliNB

from exploration.pre_processador import PreProcessador

pre_processador = PreProcessador()


def realizar_treinamento(registros_de_treino, vetorizador):
    treino_comentarios = [registro_treino[0] for registro_treino in registros_de_treino]
    treino_respostas = [registro_treino[1] for registro_treino in registros_de_treino]

    treino_comentarios = vetorizador.fit_transform(treino_comentarios)

    return BernoulliNB().fit(treino_comentarios, treino_respostas)


registros_de_treino, registros_para_avaliacao = pre_processador.pre_processamento()
vetorizador = CountVectorizer(binary='true')
classificador = realizar_treinamento(registros_de_treino, vetorizador)
resultado = classificador.predict(vetorizador.transform(["good!"]))
print(resultado)


def exibir_resultado(valor):
    frase, resultado = valor
    resultado = "Frase positiva" if resultado[0] == '1' else "Frase negativa"
    print(frase, ":", resultado)


def analisar_frase(classificador, vetorizador, frase):
    return frase, classificador.predict(vetorizador.transform([frase]))


registros_de_treino, registros_para_avaliacao = pre_processador.pre_processamento()
vetorizador = CountVectorizer(binary='true')
classificador = realizar_treinamento(registros_de_treino, vetorizador)

import pickle
with open('sentiment_labelled_sentences_model.pkl', 'wb') as fout:
  pickle.dump((vetorizador, classificador), fout)

# joblib.dump(classificador, 'sentiment_labelled_sentences_model.pkl')


exibir_resultado( analisar_frase(classificador, vetorizador, "this is the best movie"))
exibir_resultado( analisar_frase(classificador, vetorizador, "this is the worst movie"))
exibir_resultado( analisar_frase(classificador, vetorizador, "awesome!"))
exibir_resultado( analisar_frase(classificador, vetorizador,"10/10"))
exibir_resultado( analisar_frase(classificador, vetorizador,"so bad"))


def realizar_avaliacao_simples(registros_para_avaliacao):
    avaliacao_comentarios = [registro_avaliacao[0] for registro_avaliacao in registros_para_avaliacao]
    avaliacao_respostas   = [registro_avaliacao[1] for registro_avaliacao in registros_para_avaliacao]

    total = len(avaliacao_comentarios)
    acertos = 0

    for indice in range(0, total):
        resultado_analise = analisar_frase(classificador, vetorizador, avaliacao_comentarios[indice])
        frase, resultado = resultado_analise
        acertos += 1 if resultado[0] == avaliacao_respostas[indice] else 0

    return acertos * 100 / total


def realizar_avaliacao_completa(registros_para_avaliacao):
    avaliacao_comentarios = [registro_avaliacao[0] for registro_avaliacao in registros_para_avaliacao]
    avaliacao_respostas   = [registro_avaliacao[1] for registro_avaliacao in registros_para_avaliacao]

    total = len(avaliacao_comentarios)
    verdadeiros_positivos = 0
    verdadeiros_negativos = 0
    falsos_positivos = 0
    falsos_negativos = 0

    for indice in range(0, total):
        resultado_analise = analisar_frase(classificador, vetorizador, avaliacao_comentarios[indice])
        frase, resultado = resultado_analise
        if resultado[0] == '0':
            verdadeiros_negativos += 1 if avaliacao_respostas[indice] == '0' else 0
            falsos_negativos += 1 if avaliacao_respostas[indice] != '0' else 0
        else:
            verdadeiros_positivos += 1 if avaliacao_respostas[indice] == '1' else 0
            falsos_positivos += 1 if avaliacao_respostas[indice] != '1' else 0

    return ( verdadeiros_positivos * 100 / total,
             verdadeiros_negativos * 100 / total,
             falsos_positivos * 100 / total,
             falsos_negativos * 100 / total
           )


percentual_acerto = realizar_avaliacao_simples(registros_para_avaliacao)
informacoes_analise = realizar_avaliacao_completa(registros_para_avaliacao)
verdadeiros_positivos,verdadeiros_negativos,falsos_positivos,falsos_negativos = informacoes_analise

print("O modelo teve uma taxa de acerto de", percentual_acerto, "%")

print("Onde", verdadeiros_positivos, "% são verdadeiros positivos")
print("e", verdadeiros_negativos, "% são verdadeiros negativos")

print("e", falsos_positivos, "% são falsos positivos")
print("e", falsos_negativos, "% são falsos negativos")

with open('sentiment_labelled_sentences_model.pkl', 'rb') as fin:
  vectorizer, clf = pickle.load(fin)

print(analisar_frase(clf, vectorizer, 'this is the best movie'))
