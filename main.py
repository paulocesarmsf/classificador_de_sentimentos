from flask import Flask
from flask import jsonify, request

from mongo_database import configure_mongo
from workers.model_worker import ModelWorker

# comment_feeling_stopwords.pkl
# Performance
# {'precision_macro': '0.8392552655937902', macro de precisão
# 'precision_micro': '0.8120713305898491', micro de precisão
# 'precision_weighted': '0.8410491293250146'} precisão ponderada

# pontofrio_labelled_v4
# {'precision_macro': '0.8938075880758807',
# 'precision_micro': '0.8934019204389575',
# 'precision_weighted': '0.8956556295329757'}

app = Flask(__name__)
model_worker = ModelWorker()
# clf, vetorizador = model_worker.open_model('comment_feeling')
clf, vetorizador = model_worker.open_model('pontofrio_labelled_v4')


@app.route('/', methods=['GET'])
def index():
    return 'ok'


@app.route('/classify_feeling', methods=['POST'])
def classify_feeling():
    incoming_json = request.get_json()
    try:
        if incoming_json.get('text_list'):
            return jsonify(model_worker.analisar_lista_de_frases(clf, vetorizador, incoming_json.get('text_list')))
        return jsonify(model_worker.analisar_frase(clf, vetorizador, incoming_json.get('text')))
    except Exception:
        return jsonify({'success': False, 'msg': 'Error classify feelings'}), 500


if __name__ == '__main__':
    configure_mongo()
    app.run(host='0.0.0.0', port=5000, threaded=True)
